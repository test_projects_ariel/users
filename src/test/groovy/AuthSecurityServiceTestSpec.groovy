import com.globallogic.users.UsersApplication
import com.globallogic.users.exception.UsersUnauthorizedException
import com.globallogic.users.model.UserRegister
import com.globallogic.users.repository.IUserRepository
import com.globallogic.users.service.authorization.encrypt.IEncryptService
import com.globallogic.users.service.authorization.impl.SecurityServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

@SpringBootTest(classes = UsersApplication.class, properties = ["spring.sql.init.mode=never"])
class AuthSecurityServiceTestSpec extends Specification {

    @Autowired
    private IEncryptService encryptService;

    def "login check encrypted password successfully"() {
        given:
        def emailLogin = "emailLogin"
        def passwordLogin = "password12A"

        //password12A
        def encryptedPassword = "t5hzKN0QZzQOF31PBD5uYwg8RTzBzNthTes9DMElxM4"

        UserRegister userMock = new UserRegister()
        userMock.password = encryptedPassword
        userMock.email = emailLogin;

        IUserRepository userRepository = Mock()

        @Subject
        def securityServiceImpl = new SecurityServiceImpl("secretKey1234567", 60L, userRepository, encryptService);
        securityServiceImpl

        when:
        def tokenResponse = securityServiceImpl.login(emailLogin, passwordLogin)

        then:
        1 * userRepository.findByEmail(emailLogin) >> Optional.of(userMock)
    }

    def "login check invalid password return UsersUnauthorizedException"() {
        given:
        def emailLogin = "emailLogin"
        def invalidPasswordLogin = "invalidPassword12A"

        //password12A
        def encryptedPassword = "t5hzKN0QZzQOF31PBD5uYwg8RTzBzNthTes9DMElxM4"

        UserRegister userMock = new UserRegister()
        userMock.password = encryptedPassword
        userMock.email = emailLogin;

        IUserRepository userRepository = Mock()

        @Subject
        def securityServiceImpl = new SecurityServiceImpl("secretKey1234567", 60L, userRepository, encryptService);
        securityServiceImpl

        when:
        def tokenResponse = securityServiceImpl.login(emailLogin, invalidPasswordLogin)

        then:
        thrown(UsersUnauthorizedException)
        1 * userRepository.findByEmail(emailLogin) >> Optional.of(userMock)
    }


}
