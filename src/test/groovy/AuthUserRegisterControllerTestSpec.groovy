import com.fasterxml.jackson.databind.ObjectMapper
import com.globallogic.users.UsersApplication
import com.globallogic.users.dto.SignUpRequestDto
import com.globallogic.users.exception.detail.ErrorDetailDto
import com.globallogic.users.exception.enums.ErrorCode
import com.globallogic.users.service.validations.constants.ValidationsConstants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import spock.lang.Specification


@SpringBootTest(classes = UsersApplication.class)
@ContextConfiguration
@AutoConfigureMockMvc
class AuthUserRegisterControllerTestSpec extends Specification {

    @Autowired
    private MockMvc mockMvc

    @Autowired
    private ObjectMapper objectMapper

    def "validate incorrect password validations #invalidPassword"() {
        given:
        def invalidPasswordRequest = new SignUpRequestDto()
        invalidPasswordRequest.email = "ariel.zapata@gmail.com"
        invalidPasswordRequest.password = invalidPassword

        when:
        def result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidPasswordRequest)))

        then:
        result.andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect { mvcResult ->
                    def errorDetailDto = objectMapper.readValue(mvcResult.response.contentAsString, List<ErrorDetailDto>)
                    assert errorDetailDto.get(0).code == ErrorCode.VALIDATION_PARAMETER_ERROR.code
                    assert errorDetailDto.get(0).detail == ValidationsConstants.PASSWORD_INVALID_MSG

                }
        where: "passwordShort, passwordLong, passwordWithOutNumbers, passwordWithOutCapitalLetters"
        invalidPassword << ["passA12", "passwordA12ab", "passwordB", "passwordb12"]
    }

    def "test correct password #validPassword"() {
        given:
        def invalidPasswordRequest = new SignUpRequestDto()
        invalidPasswordRequest.email = "ariel.zapata2@gmail.com"
        invalidPasswordRequest.password = validPassword

        when:
        def result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidPasswordRequest)))

        then:
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())

        where: "validPasswordSize8, validPasswordSize12"
        validPassword << ["passw12B"]
    }

    def "test correct password2 #validPassword"() {
        given:
        def invalidPasswordRequest = new SignUpRequestDto()
        invalidPasswordRequest.email = "ariel.zapata@gmail.com"
        invalidPasswordRequest.password = validPassword

        when:
        def result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidPasswordRequest)))

        then:
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())

        where: "validPasswordSize8, validPasswordSize12"
        validPassword << ["passwordB12a"]
    }


    def "validate correct mails with expression successfully #mailTest"() {
        given:
        def invalidPasswordRequest = new SignUpRequestDto()
        invalidPasswordRequest.email = mailTest
        invalidPasswordRequest.password = "passwordB12a"

        when:
        def result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidPasswordRequest)))

        then:
        result.andExpect(MockMvcResultMatchers.status().is2xxSuccessful())

        where: "normalMail, mailDot, mailUnderscore"
        mailTest << ["mail@gmail.com", "mail.dot@gmail.com", "mail_underscore@gmail.com"]
    }


    def "validate invalid mails with expression successfully #invalidMailTest"() {
        given:
        def invalidPasswordRequest = new SignUpRequestDto()
        invalidPasswordRequest.email = invalidMailTest
        invalidPasswordRequest.password = "passwordB12a"

        when:
        def result = mockMvc
                .perform(MockMvcRequestBuilders.post("/auth/sign-up")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidPasswordRequest)))

        then:
        result.andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect { mvcResult ->
                    def errorDetailDto = objectMapper.readValue(mvcResult.response.contentAsString, List<ErrorDetailDto>)
                    assert errorDetailDto.get(0).code == ErrorCode.VALIDATION_PARAMETER_ERROR.code
                    assert errorDetailDto.get(0).detail == ValidationsConstants.EMAIL_INVALID_MSG

                }

        where: "mailWithOutDot, mailWithOutAt, incompleteMail"
        invalidMailTest << ["mail@gmailcom", "mail.dotgmail.com", "@gmail.com"]
    }
}
