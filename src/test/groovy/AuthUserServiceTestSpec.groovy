import com.globallogic.users.UsersApplication
import com.globallogic.users.dto.PhoneDto
import com.globallogic.users.dto.SignInRequestDto
import com.globallogic.users.dto.SignUpRequestDto
import com.globallogic.users.mapper.IAuthUserMapper
import com.globallogic.users.model.UserRegister
import com.globallogic.users.repository.IUserRepository
import com.globallogic.users.service.authorization.ISecurityService
import com.globallogic.users.service.impl.AuthUserServiceImpl
import com.globallogic.users.service.validations.IAuthServiceValidation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

import java.time.LocalDateTime


@SpringBootTest(classes = UsersApplication.class, properties = ["spring.sql.init.mode=never"])
class AuthUserServiceTestSpec extends Specification {

    @Autowired
    private IAuthUserMapper authUserMapper;

    def "signUp new user successfully check save data"() {
        given:
        def phoneNumber = 12345678

        def validSignUp = new SignUpRequestDto()
        validSignUp.email = "ariel.zapata@gmail.com"
        validSignUp.password = "password12A"
        validSignUp.phones = new ArrayList<>()
        validSignUp.phones.add(new PhoneDto(phoneNumber, 11, "+54"))

        IUserRepository userRepository = Mock()
        IAuthServiceValidation authServiceValidation = Mock()
        ISecurityService securityService = Mock()

        def passwordEncrypted = "passwordEncrypted"

        @Subject
        def serviceImpl = new AuthUserServiceImpl(userRepository, authServiceValidation, securityService, authUserMapper)

        when:
        def serviceResponse = serviceImpl.signUpUser(validSignUp)

        then:
        1 * userRepository.save({
            it.email == validSignUp.email
            it.password == passwordEncrypted
            assert it.phones[0].number == phoneNumber
        })
        serviceResponse.isActive == true
        1 * securityService.encryptUserPassword(_) >> passwordEncrypted
    }


    def "signIn user successfully"() {
        given:
        def validSignIn = new SignInRequestDto()
        validSignIn.email = "ariel.zapata@gmail.com"
        validSignIn.password = "password12B"

        def user = new UserRegister()
        user.password = validSignIn.password
        user.createdDate = LocalDateTime.now()
        user.name = "nameTest"
        user.isActive = true

        IUserRepository userRepository = Mock()
        IAuthServiceValidation authServiceValidation = Mock()
        ISecurityService securityService = Mock()

        def mockToken = "token12345678"

        @Subject
        def serviceImpl = new AuthUserServiceImpl(userRepository, authServiceValidation, securityService, authUserMapper)

        when:
        def serviceResponse = serviceImpl.signInUser(validSignIn)

        then:
        serviceResponse.created == user.createdDate
        serviceResponse.name == user.name
        serviceResponse.isActive == true
        serviceResponse.token == mockToken
        1 * securityService.login(validSignIn.email, validSignIn.password) >> mockToken
        1 * userRepository.findByEmail(validSignIn.email) >> Optional.of(user)
    }
}
