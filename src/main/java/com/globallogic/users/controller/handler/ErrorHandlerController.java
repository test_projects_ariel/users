package com.globallogic.users.controller.handler;

import com.globallogic.users.exception.UsersUnauthorizedException;
import com.globallogic.users.exception.UsersValidationException;
import com.globallogic.users.exception.detail.ErrorDetailDto;
import com.globallogic.users.exception.enums.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ErrorHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandlerController.class);


    @ExceptionHandler(UsersValidationException.class)
    public ResponseEntity<List<ErrorDetailDto>> handleValidationException(UsersValidationException ex) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(mapToError(ex.getErrors()));
    }

    @ExceptionHandler(UsersUnauthorizedException.class)
    public ResponseEntity<List<ErrorDetailDto>> handleUnauthorizedException(UsersUnauthorizedException ex) {
        List<ErrorCode> genericError = new ArrayList<>();
        genericError.add(ErrorCode.UNAUTHORIZED);

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(mapToError(genericError));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<List<ErrorDetailDto>> handleGenericException(Exception ex) {
        List<ErrorCode> genericError = new ArrayList<>();

        //TODO SEND EXCEPTION ERROR TO A SLACK OR OTHER PLACE!!!!
        LOGGER.error(ex.getMessage(), ex);

        genericError.add(ErrorCode.INTERNAL_ERROR);

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(mapToError(genericError));


    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorDetailDto>> handleArgumentValidationException(MethodArgumentNotValidException ex) {
        LocalDateTime timestamp = LocalDateTime.now();

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(ex.getBindingResult().getFieldErrors().stream().map(
                        fieldError ->
                                ErrorDetailDto.builder()
                                        .code(ErrorCode.VALIDATION_PARAMETER_ERROR.getCode())
                                        .detail(fieldError.getDefaultMessage())
                                        .timestamp(timestamp).build()
                ).collect(Collectors.toList()));
    }

    public List<ErrorDetailDto> mapToError(List<ErrorCode> errors) {
        LocalDateTime timestamp = LocalDateTime.now();

        return errors.stream().map(error -> ErrorDetailDto.builder()
                .detail(error.getDescription())
                .timestamp(timestamp)
                .code(error.getCode()).build()

        ).collect(Collectors.toList());
    }
}
