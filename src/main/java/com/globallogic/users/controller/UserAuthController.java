package com.globallogic.users.controller;

import com.globallogic.users.service.IAuthUserService;
import com.globallogic.users.dto.SignInRequestDto;
import com.globallogic.users.dto.SignInResponseDto;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.dto.SignUpResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/auth")
@RestController
@RequiredArgsConstructor
public class UserAuthController {

    private final IAuthUserService authUserService;


    @PostMapping(path = "sign-up", consumes = "application/json", produces = "application/json")
    public SignUpResponseDto signUp(@Valid @RequestBody SignUpRequestDto request) {
        return authUserService.signUpUser(request);
    }

    @PostMapping(path = "login", consumes = "application/json", produces = "application/json")
    public SignInResponseDto login(@RequestBody @Valid SignInRequestDto signInRequestDto) {
        return authUserService.signInUser(signInRequestDto);
    }


}
