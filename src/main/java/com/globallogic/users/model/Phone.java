package com.globallogic.users.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "phone")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Phone {

    @Id
    @Column(name = "phone_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long phoneId;

    @Column(name = "number")
    @NotNull()
    private Long number;

    @NotNull
    @Column(name = "city_code")
    private Integer cityCode;

    @NotBlank
    @Column(name = "country_code")
    private String countryCode;

    @ManyToOne()
    @JoinColumn(name = "user_register_id")
    private UserRegister user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(phoneId, phone.phoneId) && Objects.equals(number, phone.number) && Objects.equals(cityCode, phone.cityCode) && Objects.equals(countryCode, phone.countryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneId, number, cityCode, countryCode);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "phoneId=" + phoneId +
                ", number=" + number +
                ", cityCode=" + cityCode +
                ", countryCode='" + countryCode + '\'' +
                ", user=" + user.getUserRegisterId() +
                '}';
    }
}
