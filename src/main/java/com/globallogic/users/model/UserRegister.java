package com.globallogic.users.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "user_register")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRegister {

    @Id
    @Column(name = "user_register_id")
    private String userRegisterId;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "last_login_date")
    private LocalDateTime lastLoginDate;

    @OneToMany(mappedBy = "user", cascade = CascadeType.MERGE)
    private List<Phone> phones;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRegister user = (UserRegister) o;
        return Objects.equals(userRegisterId, user.userRegisterId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userRegisterId);
    }

    @Override
    public String toString() {
        return "User{" +
                "userRegisterId='" + userRegisterId + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", createdDate=" + createdDate +
                ", isActive=" + isActive +
                ", lastLoginDate=" + lastLoginDate +
                ", phones=" + phones +
                '}';
    }
}

