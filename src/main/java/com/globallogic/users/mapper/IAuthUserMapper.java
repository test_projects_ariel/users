package com.globallogic.users.mapper;

import com.globallogic.users.model.Phone;
import com.globallogic.users.model.UserRegister;
import com.globallogic.users.dto.PhoneDto;
import com.globallogic.users.dto.SignInResponseDto;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.dto.SignUpResponseDto;

public interface IAuthUserMapper {

    UserRegister mapToUserEntity(SignUpRequestDto signUpRequest, String encryptedPassword);

    Phone mapToPhoneEntity(PhoneDto phoneDto, UserRegister user);

    SignUpResponseDto mapToSignUpResponseDto(UserRegister user, String decryptedPassword);

    SignInResponseDto mapToSignInResponseDto(UserRegister user, String token, String decryptedPass);
}
