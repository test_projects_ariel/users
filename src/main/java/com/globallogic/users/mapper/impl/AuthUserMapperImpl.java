package com.globallogic.users.mapper.impl;

import com.globallogic.users.mapper.IAuthUserMapper;
import com.globallogic.users.model.Phone;
import com.globallogic.users.model.UserRegister;
import com.globallogic.users.dto.PhoneDto;
import com.globallogic.users.dto.SignInResponseDto;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.dto.SignUpResponseDto;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AuthUserMapperImpl implements IAuthUserMapper {

    @Override
    public UserRegister mapToUserEntity(SignUpRequestDto signUpRequest, String encryptedPassword) {
        return UserRegister.builder()
                .userRegisterId(UUID.randomUUID().toString())
                .email(signUpRequest.getEmail())
                .password(encryptedPassword)
                .name(signUpRequest.getName())
                .isActive(true)
                .lastLoginDate(LocalDateTime.now())
                .createdDate(LocalDateTime.now()).build();
    }

    @Override
    public Phone mapToPhoneEntity(PhoneDto phoneDto, UserRegister user) {
        return Phone.builder()
                .countryCode(phoneDto.getCountryCode())
                .cityCode(phoneDto.getCityCode())
                .number(phoneDto.getNumber())
                .user(user).build();
    }

    @Override
    public SignUpResponseDto mapToSignUpResponseDto(UserRegister user, String decryptedPassword) {
        return SignUpResponseDto.builder()
                .userId(user.getUserRegisterId())
                .isActive(user.getIsActive())
                .created(user.getCreatedDate())
                .lastLogin(user.getLastLoginDate())
                .token(decryptedPassword)
                .build();
    }

    @Override
    public SignInResponseDto mapToSignInResponseDto(UserRegister user, String token, String decryptedPass) {
        SignInResponseDto.SignInResponseDtoBuilder builder = SignInResponseDto.builder()
                .id(user.getUserRegisterId())
                .name(user.getName())
                .isActive(user.getIsActive())
                .created(user.getCreatedDate())
                .lastLogin(user.getLastLoginDate())
                .password(decryptedPass)
                .token(token);

        if (Objects.nonNull(user.getPhones())) {
            builder.phones(user.getPhones().stream().map(
                    phoneEntity -> PhoneDto.builder()
                            .countryCode(phoneEntity.getCountryCode())
                            .cityCode(phoneEntity.getCityCode())
                            .number(phoneEntity.getNumber()).build()
            ).collect(Collectors.toList()));
        }

        return builder.build();
    }
}
