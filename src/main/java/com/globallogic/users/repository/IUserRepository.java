package com.globallogic.users.repository;

import com.globallogic.users.model.UserRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<UserRegister, Long> {

    @Query("SELECT u FROM UserRegister u WHERE u.email = :email")
    Optional<UserRegister> findByEmail(String email);
}
