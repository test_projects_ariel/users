package com.globallogic.users.repository;

import com.globallogic.users.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IPhoneRepository extends JpaRepository<Phone, Long> {

    @Query("SELECT COUNT(*) FROM Phone p WHERE p.cityCode = :cityCode AND p.countryCode = :countryCode AND p.number = :phoneNumber")
    Integer findByUK(String countryCode, Integer cityCode, Long phoneNumber);
}
