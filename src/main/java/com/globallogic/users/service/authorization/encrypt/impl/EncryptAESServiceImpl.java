package com.globallogic.users.service.authorization.encrypt.impl;

import com.globallogic.users.service.authorization.encrypt.IEncryptService;
import com.globallogic.users.exception.UsersInternalException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

@Service
public class EncryptAESServiceImpl implements IEncryptService {

    private static final String ALGORITHM = "AES";
    private static final String CYPHER_TYPE_INSTANCE = "AES/CBC/PKCS5Padding";

    @Value("${app.aes.secret-key}")
    private String secretKey;

    @Override
    public String encrypt(String value) {
        try {
            SecretKey secretKey = secretKey();

            // Random IV
            SecureRandom secureRandom = new SecureRandom();
            byte[] ivBytes = new byte[16];
            secureRandom.nextBytes(ivBytes);
            IvParameterSpec iv = new IvParameterSpec(ivBytes);

            Cipher cipher = Cipher.getInstance(CYPHER_TYPE_INSTANCE);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

            // Encrypt the data
            byte[] encryptedBytes = cipher.doFinal(value.getBytes());

            // Combine the IV and encrypted data
            byte[] combined = new byte[ivBytes.length + encryptedBytes.length];
            System.arraycopy(ivBytes, 0, combined, 0, ivBytes.length);
            System.arraycopy(encryptedBytes, 0, combined, ivBytes.length, encryptedBytes.length);

            return Base64.getEncoder().encodeToString(combined);
        } catch (Exception e) {
            throw new UsersInternalException(e);
        }
    }


    @Override
    public String decrypt(String encryptedValue) {
        try {
            SecretKey secretKey = secretKey();

            // Takes the IV from the encrypted value
            byte[] combined = Base64.getDecoder().decode(encryptedValue);
            byte[] ivBytes = Arrays.copyOfRange(combined, 0, 16);
            IvParameterSpec iv = new IvParameterSpec(ivBytes);

            Cipher cipher = Cipher.getInstance(CYPHER_TYPE_INSTANCE);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);

            // Decrypt the actual encrypted data (excluding the IV)
            byte[] encryptedData = Arrays.copyOfRange(combined, 16, combined.length);
            byte[] decryptedBytes = cipher.doFinal(encryptedData);

            return new String(decryptedBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new UsersInternalException(e);
        }
    }


    public SecretKey secretKey() {
        byte[] keyBytes = secretKey.getBytes(StandardCharsets.UTF_8);
        return new SecretKeySpec(keyBytes, ALGORITHM);
    }

}
