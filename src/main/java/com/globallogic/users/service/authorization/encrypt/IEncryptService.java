package com.globallogic.users.service.authorization.encrypt;


public interface IEncryptService {

    String encrypt(String value);

    String decrypt(String value);

}
