package com.globallogic.users.service.authorization.impl;

import com.globallogic.users.model.UserRegister;
import com.globallogic.users.repository.IUserRepository;
import com.globallogic.users.service.authorization.ISecurityService;
import com.globallogic.users.service.authorization.encrypt.IEncryptService;
import com.globallogic.users.exception.UsersUnauthorizedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Service
public class SecurityServiceImpl implements ISecurityService {

    private final String secretKey;


    private final Long accessTokenExpiration;

    private final IUserRepository userRepository;

    private final IEncryptService encryptService;

    public SecurityServiceImpl(@Value("${app.authorization.secret-key}") String secretKey,
                               @Value("${app.authorization.token-expiration}") Long accessTokenExpiration,
                               IUserRepository userRepository, IEncryptService encryptService) {
        this.secretKey = secretKey;
        this.accessTokenExpiration = accessTokenExpiration;
        this.userRepository = userRepository;
        this.encryptService = encryptService;

    }

    private String createToken(String userEmail) {
        Claims claims = Jwts.claims().setSubject(userEmail);
        claims.put("email", userEmail);
        Date tokenExpirationDate = new Date(new Date().getTime() + TimeUnit.MINUTES.toMillis(accessTokenExpiration));
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(tokenExpirationDate)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public String login(String email, String password) {
        UserRegister user = userRepository.findByEmail(email).orElseThrow(() -> new UsersUnauthorizedException("User does not exist"));

        String userPassword = encryptService.decrypt(user.getPassword());

        if (userPassword.equals(password)) {
            return this.createToken(email);
        } else {
            throw new UsersUnauthorizedException("Invalid password");
        }
    }

    @Override
    public String encryptUserPassword(String password) {
        return encryptService.encrypt(password);
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User does not exist"));

        return new org.springframework.security.core.userdetails.User(
                email,
                "",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
    }
}
