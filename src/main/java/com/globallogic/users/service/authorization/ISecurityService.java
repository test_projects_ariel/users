package com.globallogic.users.service.authorization;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface ISecurityService extends UserDetailsService {

    String login(String email, String password);

    String encryptUserPassword(String password);
}
