package com.globallogic.users.service.validations.impl;

import com.globallogic.users.repository.IPhoneRepository;
import com.globallogic.users.repository.IUserRepository;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.exception.UsersValidationException;
import com.globallogic.users.exception.enums.ErrorCode;
import com.globallogic.users.service.validations.IAuthServiceValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Validated
public class AuthServiceValidationImpl implements IAuthServiceValidation {

    private final IUserRepository userRepository;

    private final IPhoneRepository phoneRepository;

    @Override
    public void validateSignUp(SignUpRequestDto signUpRequest) {

        userRepository.findByEmail(signUpRequest.getEmail()).ifPresent(
                user -> {
                    throw new UsersValidationException(ErrorCode.EMAIL_ALREADY_EXISTS, "User already exists");
                }
        );

        if (Objects.nonNull(signUpRequest.getPhones()) && !signUpRequest.getPhones().isEmpty()) {
            signUpRequest.getPhones().forEach(phoneDto -> {
                        Integer phoneCount = phoneRepository.findByUK(phoneDto.getCountryCode(), phoneDto.getCityCode(), phoneDto.getNumber());
                        if (phoneCount > 0) {
                            throw new UsersValidationException(ErrorCode.PHONE_ALREADY_EXISTS, "Phone already exists");
                        }
                    }
            );
        }
    }
}
