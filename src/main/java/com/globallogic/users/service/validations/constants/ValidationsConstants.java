package com.globallogic.users.service.validations.constants;

public class ValidationsConstants {
    public static final String EMAIL_PATTERN = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$";
    public static final String PASSWORD_PATTERN = "^(?=(?:[^A-Z]*[A-Z]){1})(?=(?:.*\\d.*){2})(?=.*[a-z])[A-Za-z\\d]{8,12}$";
    public static final String EMAIL_INVALID_MSG = "invalid email";
    public static final String PASSWORD_INVALID_MSG = "password must have one capital letter, two numbers and size between 8 an 12 characters";
    public static final String COUNTRY_CODE_INVALID_MSG = "Invalid country code";
    public static final String CITY_CODE_INVALID_MSG = "Invalid city code";
    public static final String PHONE_NUMBER_INVALID_MSG = "Invalid phone number";
}
