package com.globallogic.users.service.validations;

import com.globallogic.users.dto.SignUpRequestDto;

public interface IAuthServiceValidation {

    void validateSignUp(SignUpRequestDto signUpRequest);


}
