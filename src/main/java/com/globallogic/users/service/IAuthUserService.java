package com.globallogic.users.service;

import com.globallogic.users.dto.SignInRequestDto;
import com.globallogic.users.dto.SignInResponseDto;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.dto.SignUpResponseDto;

public interface IAuthUserService {

    SignUpResponseDto signUpUser(SignUpRequestDto signUpRequest);

    SignInResponseDto signInUser(SignInRequestDto signInRequestDto);


}
