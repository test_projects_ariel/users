package com.globallogic.users.service.impl;

import com.globallogic.users.dto.SignInRequestDto;
import com.globallogic.users.dto.SignInResponseDto;
import com.globallogic.users.dto.SignUpRequestDto;
import com.globallogic.users.dto.SignUpResponseDto;
import com.globallogic.users.model.Phone;
import com.globallogic.users.model.UserRegister;
import com.globallogic.users.repository.IUserRepository;
import com.globallogic.users.service.IAuthUserService;
import com.globallogic.users.service.authorization.ISecurityService;
import com.globallogic.users.exception.UsersUnauthorizedException;
import com.globallogic.users.mapper.IAuthUserMapper;
import com.globallogic.users.service.validations.IAuthServiceValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class AuthUserServiceImpl implements IAuthUserService {

    private final IUserRepository userRepository;

    private final IAuthServiceValidation authServiceValidation;

    private final ISecurityService securityService;

    private final IAuthUserMapper authUserMapper;

    @Override
    @Transactional
    public SignUpResponseDto signUpUser(SignUpRequestDto signUpRequest) {
        authServiceValidation.validateSignUp(signUpRequest);

        UserRegister user = authUserMapper.mapToUserEntity(signUpRequest, securityService.encryptUserPassword(signUpRequest.getPassword()));

        if (Objects.nonNull(signUpRequest.getPhones()) && !signUpRequest.getPhones().isEmpty()) {
            List<Phone> phones = signUpRequest.getPhones().stream()
                    .map(phoneDto -> authUserMapper.mapToPhoneEntity(phoneDto, user)
                    ).collect(Collectors.toList());
            user.setPhones(phones);

        }
        userRepository.save(user);

        return authUserMapper.mapToSignUpResponseDto(user, securityService.login(signUpRequest.getEmail(), signUpRequest.getPassword()));

    }

    @Override
    public SignInResponseDto signInUser(SignInRequestDto signInRequestDto) {
        String token = securityService.login(signInRequestDto.getEmail(), signInRequestDto.getPassword());

        UserRegister user = userRepository.findByEmail(signInRequestDto.getEmail())
                .orElseThrow(() -> new UsersUnauthorizedException("user does not exists"));

        user.setLastLoginDate(LocalDateTime.now());
        userRepository.save(user);


        return authUserMapper.mapToSignInResponseDto(user, token, signInRequestDto.getPassword());
    }

}
