package com.globallogic.users.dto;

import com.globallogic.users.service.validations.constants.ValidationsConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PhoneDto {

    @NotNull(message = ValidationsConstants.PHONE_NUMBER_INVALID_MSG)
    private Long number;

    @NotNull(message = ValidationsConstants.CITY_CODE_INVALID_MSG)
    private Integer cityCode;

    @NotBlank(message = ValidationsConstants.COUNTRY_CODE_INVALID_MSG)
    private String countryCode;
}
