package com.globallogic.users.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class SignInResponseDto {

    private String id;

    private LocalDateTime created;

    private LocalDateTime lastLogin;

    private String token;

    private Boolean isActive;

    private String name;

    private String password;

    private List<PhoneDto> phones;


}
