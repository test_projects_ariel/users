package com.globallogic.users.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class SignUpResponseDto {

    private String userId;

    private LocalDateTime created;

    private LocalDateTime lastLogin;

    private Boolean isActive;

    private String token;
}
