package com.globallogic.users.dto;

import com.globallogic.users.service.validations.constants.ValidationsConstants;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
public class SignUpRequestDto {

    private String name;

    @NotBlank(message = ValidationsConstants.EMAIL_INVALID_MSG)
    @Pattern(regexp = ValidationsConstants.EMAIL_PATTERN, message = ValidationsConstants.EMAIL_INVALID_MSG)
    private String email;

    @Pattern(regexp = ValidationsConstants.PASSWORD_PATTERN, message = ValidationsConstants.PASSWORD_INVALID_MSG)
    @NotBlank(message = ValidationsConstants.PASSWORD_INVALID_MSG)
    private String password;

    @Valid
    private List<PhoneDto> phones;
}
