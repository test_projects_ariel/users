package com.globallogic.users.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class UsersInternalException extends RuntimeException {

    public UsersInternalException(Throwable exception) {
        super(exception);
    }

}
