package com.globallogic.users.exception.detail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class ErrorDetailDto {
    private Integer code;
    private String detail;
    private LocalDateTime timestamp;
}
