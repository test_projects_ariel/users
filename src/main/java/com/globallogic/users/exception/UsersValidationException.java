package com.globallogic.users.exception;

import com.globallogic.users.exception.enums.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class UsersValidationException extends RuntimeException {

    private List<ErrorCode> errors;

    public UsersValidationException(List<ErrorCode> errors, String internalMsg) {
        super(internalMsg);
        this.errors = errors;
    }

    public UsersValidationException(ErrorCode error, String internalMsg) {
        super(internalMsg);
        this.errors = new ArrayList<>();
        this.errors.add(error);

    }

}
