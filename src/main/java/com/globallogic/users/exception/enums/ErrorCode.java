package com.globallogic.users.exception.enums;

import lombok.Getter;

@Getter
public enum ErrorCode {

    EMAIL_ALREADY_EXISTS(1, "Email already exists"),
    PHONE_ALREADY_EXISTS(2, "Phone already exists"),
    VALIDATION_PARAMETER_ERROR(400, "Validation Error"),
    INTERNAL_ERROR(500, "Internal Error"),
    UNAUTHORIZED(401,"Unauthorized Error");

    private final Integer code;
    private final String description;

    ErrorCode(Integer code, String description) {
        this.code = code;
        this.description = description;
    }
}