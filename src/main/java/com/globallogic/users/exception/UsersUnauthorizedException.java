package com.globallogic.users.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class UsersUnauthorizedException extends RuntimeException {

    public UsersUnauthorizedException(String errorMsg) {
        super(errorMsg);
    }

}
