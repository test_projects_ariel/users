CREATE TABLE "USER_REGISTER" (
    user_register_id VARCHAR(64) PRIMARY KEY,
    name VARCHAR(50),
    email VARCHAR(60) NOT NULL ,
    password VARCHAR(150) NOT NULL,
    created_date TIMESTAMP NOT NULL,
    is_active BOOLEAN NOT NULL,
    last_login_date TIMESTAMP NOT NULL
);

ALTER TABLE "USER_REGISTER" ADD CONSTRAINT uk_register_user_email UNIQUE (email);

CREATE TABLE phone (
    phone_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    number BIGINT NOT NULL ,
    city_code INT NOT NULL ,
    country_code VARCHAR(10) NOT NULL,
    user_register_id VARCHAR(64) NOT NULL
);

ALTER TABLE phone ADD CONSTRAINT fk_user FOREIGN KEY (user_register_id) REFERENCES "USER_REGISTER" (user_register_id);
ALTER TABLE phone ADD CONSTRAINT uk_phone UNIQUE (number, city_code, country_code);