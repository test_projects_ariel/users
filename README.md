# Getting Started

## Build and run with docker
``` 
1) cd pathToProject
2) add execution permission to script gradlew with: chmod +x gradlew
3) ./gradlew clean build
4) docker build -t users .
5) docker run -p 8080:8080 users
``` 

## Build and run with java 8
``` 
1) cd pathToProject
2) add execution permission to script gradlew with:c hmod +x gradlew
3) ./gradlew clean build
4) java -jar build/libs/users-0.0.1-SNAPSHOT.jar
```
## BD H2 Schema
[schema.sql](src/main/resources/schema.sql) 

## Diagrams
### BD
![Alt text](diagrams/bd_diagram.jpeg)

### UML SignIn
![Alt text](diagrams/SignIn.png)

### UML SignUp
![Alt text](diagrams/SignUp.png)


## Example curls
### signUp
``` 
curl --location 'http://localhost:8080/auth/sign-up' \
--header 'Content-Type: application/json' \
--data-raw '{
"name": "Esteban zurita",
"email": "esteban.zurita@gmail.com",
"password": "password12A",
"phones": [{
   "number": 114,
   "cityCode": 11,
   "countryCode": "54"
}]}'
```

### signIn
```
curl --location 'http://localhost:8080/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{

     "email": "esteban.zurita@gmail.com",
     "password": "password12A"
}'
```