# Use Amazon Corretto 17 as the base image
FROM amazoncorretto:8

# Set the working directory in the container
WORKDIR /app

# Copy the Java application JAR file into the container
COPY build/libs/users*.jar app.jar

# Define the entry point to run your Java application
CMD ["java", "-jar", "app.jar"]

EXPOSE 8080